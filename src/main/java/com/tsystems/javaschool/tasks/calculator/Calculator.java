package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashSet;
import java.util.Set;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if (statement == null || statement.equals("") || !checkString(statement) || checkBrackets(statement)) return null;

        char[] character = statement.toCharArray();
        StringBuilder out = new StringBuilder();
        StringBuilder tempNumb = new StringBuilder();
        Stack<String> stack = new Stack<>();

        for (int i = 0; i < statement.length(); i++) {
            if (Character.isDigit(character[i]) || character[i] == '.') {
                tempNumb.append(character[i]);
                continue;
            }
            if (tempNumb.length() > 0) {
                out.append(tempNumb).append(" ");
                tempNumb.setLength(0);
            }

            if (character[i] == '(') {
                stack.push(String.valueOf(character[i]));
            } else if (character[i] == ')') {
                if (stack.len() > 0) {
                    String func = stack.pop();
                    while (!func.equals("(")) {
                        out.append(func).append(" ");
                        func = stack.pop();
                    }
                }
            } else {
                while (stack.len() > 0 && priority(stack.peek()) >= priority(String.valueOf(character[i]))) {
                    out.append(stack.pop()).append(" ");
                }
                stack.push(String.valueOf(character[i]));
            }
        }

        if (tempNumb.length() > 0) {
            out.append(tempNumb).append(" ");
            tempNumb.setLength(0);
        }
        while (stack.len() > 0) {
            out.append(stack.pop()).append(" ");
        }

        String[] output = out.toString().split(" ");
        for (String s : output) {
            if (isDouble(s)) {
                stack.push(s);
            } else {
                double b = Double.parseDouble(stack.pop());
                double a = Double.parseDouble(stack.pop());
                double temp = 0;
                switch (s) {
                    case "+":
                        temp = a + b;
                        break;
                    case "-":
                        temp = a - b;
                        break;
                    case "*":
                        temp = a * b;
                        break;
                    case "/":
                        if (b == 0) return null;
                        temp = a / b;
                        break;
                }
                stack.push(String.valueOf(temp));
            }
        }
        double dotIndex = Double.parseDouble(stack.peek());
        String result;
        if (dotIndex % 1 == 0) {
            result = String.valueOf((int) dotIndex);
        } else {
            DecimalFormatSymbols s = new DecimalFormatSymbols();
            s.setDecimalSeparator('.');
            DecimalFormat f = new DecimalFormat("0.0000", s);
            result = trimZero(f.format(dotIndex));
        }
        return result;
    }

    private boolean checkString(String input) {
        Set<String> chars = new HashSet<>();
        chars.add("++");
        chars.add("--");
        chars.add("**");
        chars.add("//");
        chars.add("..");
        chars.add(" ");
        chars.add(",");

        for (String str : chars
        ) {
            if (input.contains(str)) return false;
        }
        return true;
    }

    private boolean checkBrackets(String input){
        int countBrackets=0;
        for (int i = 0; i < input.length(); i++) {
            switch(input.charAt(i)){
                case '(':
                    countBrackets++;
                    break;
                case ')':
                    countBrackets--;
                    break;
            }
            if (countBrackets<0) {
                return true;
            }
        }
        return countBrackets != 0;
    }

    private boolean isDouble(String maybeDouble) {
        try {
            Double.parseDouble(maybeDouble);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private String trimZero(String str) {
        int index = 0;
        int lengthStr = str.length();
        while (true) {
            if (str.charAt(lengthStr - index - 1) == '0') {
                index++;
            } else break;
        }
        int len = str.length() - index;
        return str.substring(0, len);
    }

    private int priority(String c) {
        switch (c) {
            case "*":
            case "/":
                return 3;
            case "+":
            case "-":
                return 2;
            default:
                return 0;
        }
    }

}
