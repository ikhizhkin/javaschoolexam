package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;
import java.util.List;

class Stack<T> {
    private List<T> stack = new LinkedList<>();

    void push(T item) {
        stack.add(item);
    }

    T pop() {
        return stack.remove(stack.size() - 1);
    }

    T peek() {
        return stack.get(stack.size() - 1);
    }

    int len() {
        return stack.size();
    }
}
