package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) throw new IllegalArgumentException();

        int index = 0;

        for (Object xElem : x) {
            if (!y.contains(xElem)) {
                return false;
            }
            int lastIndex = index;
            for (int j = index; j < y.size(); j++) {
                if (y.get(j).equals(xElem)) {
                    index = j + 1;
                    break;
                }
            }
            if (lastIndex == index) {
                return false;
            }
        }

        return true;
    }
}
