package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    int[][] buildPyramid(List<Integer> inputNumbers) {

        checkList(inputNumbers);

        try {
            inputNumbers.sort(Comparator.naturalOrder());
        } catch (OutOfMemoryError e) {
            throw new CannotBuildPyramidException();
        }

        int countRows = findCountRows(inputNumbers);
        int countDigits = 2 * countRows - 1;

        int[][] pyramid = new int[countRows][countDigits];

        fillPyramid(pyramid, inputNumbers, countDigits, countRows);

        return pyramid;
    }

    private int findCountRows(List<Integer> inputNumbers) throws CannotBuildPyramidException {
        int countNumbers = inputNumbers.size();

        int rows = 1;
        while (true) {
            int k = (int) (Math.pow(rows, 2) * 0.5 + rows * 0.5);
            if (k == countNumbers) return rows;
            if (k > countNumbers) {
                throw new CannotBuildPyramidException();
            }
            rows++;
        }
    }

    private void fillPyramid(int[][] pyramid, List<Integer> inputNumbers, int countDigits, int countRows) {
        int index = countDigits / 2;
        List<Integer> a = new ArrayList<>();
        List<Integer> input = new ArrayList<>(inputNumbers);
        int m;

        for (int i = 0; i < countRows; i++) {
            m = i;

            if (m == 0) {
                a.add(index);
            }
            if (m == 1) {
                a.add(index - m);
                a.add(index + m);
            }
            if (m != 1 && m % 2 != 0) {
                while (m > 0) {
                    a.add(index - m);
                    a.add(index + m);
                    m = m - 2;
                }
            } else if (m != 0 && m % 2 == 0) {
                a.add(index);
                while (m > 0) {
                    a.add(index - m);
                    a.add(index + m);
                    m = m - 2;
                }
            }
            a.sort(Comparator.naturalOrder());
            for (int j = 0; j < countDigits; j++) {
                if (a.contains(j)) {
                    pyramid[i][j] = input.get(0);
                    a.remove(0);
                    input.remove(0);
                } else pyramid[i][j] = 0;
            }
        }
    }

    private void checkList(List<Integer> inputNumbers) {
        for (Object i : inputNumbers
        ) {
            if (i == null)
                throw new CannotBuildPyramidException();
        }
    }

}
